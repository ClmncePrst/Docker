# Docker : Persistance des Données

## La persistance des Données dans Docker en résumé

### Les données persistent-elles lorsque qu'on stoppe et redémarre un conteneur ?

Non, par défaut, les données à l'intérieur d'un conteneur Docker ne persistent pas après l'arrêt de celui-ci. Chaque fois que le conteneur est stoppé, toutes les modifications effectuées à l'intérieur du conteneur sont perdues (nature éphémère des conteneurs).

### Qu'est-ce qu'un volume ?

Un volume en Docker est un moyen de stocker et de partager des données entre les conteneurs et l'hôte. Les volumes sont des entités indépendantes des conteneurs, ce qui signifie que les données qu'ils contiennent ne sont pas liées à la durée de vie d'un conteneur spécifique. Les volumes sont idéaux pour la persistance des données, même lorsque les conteneurs associés sont supprimés ou redémarrés.

Les volumes Docker peuvent être utilisés pour stocker des bases de données, des fichiers de configuration, des journaux, et d'autres données qui doivent survivre au cycle de vie d'un conteneur.

### Qu'est-ce qu'un bind mount, et quelle est la différence avec un volume ?

Un bind mount est une autre méthode pour partager des données entre un conteneur et l'hôte ou entre plusieurs conteneurs. Contrairement aux volumes, les bind mounts sont basés sur le montage direct des répertoires de l'hôte dans le conteneur. Cela signifie que les fichiers du système hôte sont accessibles et partagés avec le conteneur en temps réel.

La principale différence entre les volumes et les bind mounts réside dans leur portée et leur gestion. Les volumes sont gérés par Docker et sont spécifiques à Docker, tandis que les bind mounts dépendent du système de fichiers de l'hôte.

En résumé, les volumes sont recommandés pour la persistance des données, car ils offrent une meilleure isolation et portabilité entre les conteneurs, tandis que les bind mounts sont plus simples à mettre en œuvre, mais dépendent étroitement du système de fichiers de l'hôte.

## Volumes Docker

1. **Qu'est-ce qu'un Volume Docker ?** En Docker, un volume est un moyen de stocker et de partager des données entre les conteneurs Docker et l'hôte. Contrairement au système de fichiers du conteneur, les volumes sont indépendants du cycle de vie du conteneur, ce qui signifie que les données qu'ils contiennent persistent même lorsque le conteneur est supprimé ou redémarré. Ils sont essentiels pour la persistance des données et le partage entre les conteneurs. Leur gestion facile et leur indépendance par rapport aux conteneurs les rendent cruciaux dans de nombreux scénarios d'utilisation de Docker.
2. **Création de Volumes:** Les volumes Docker peuvent être créés explicitement lors du lancement d'un conteneur ou en utilisant la commande `docker volume create <mon_volume>`
3. **Utilisation de Volumes:** Les volumes peuvent être montés dans un conteneur lors de son lancement. On spécifie le nom du volume et le chemin de montage dans le conteneur. Par exemple :
4. **Volumes Prédéfinis:** Docker propose également des volumes prédéfinis pour certaines utilisations spécifiques. Par exemple, le volume `docker run -v /chemin/dans/conteneur` crée un volume anonyme, sans spécifier de nom.
    
    ```markdown
    docker run -v mon_volume:/chemin/dans/conteneur mon_image
    ```
    
5. **Liste des Volumes:** Pour afficher la liste des volumes sur le système, on peut utiliser la commande `docker volume ls`.
6. **Suppression des Volumes:** Pour supprimer un volume, on peut utiliser la commande `docker volume rm <mon_volume>`.
7. **Avantages des Volumes: 
Persistance des Données :** Les données dans un volume ne sont pas liées à la vie du conteneur.
**Isolation :** Les volumes sont gérés par Docker, offrant une isolation et une portabilité accrues.
**Partage de Données :** Les volumes facilitent le partage de données entre les conteneurs.
8. **Exemple complet de Volume:** 

```markdown
### Création d'un volume
docker volume create mon_volume

### Lancement d'un conteneur avec le volume monté
docker run -v mon_volume:/chemin/dans/conteneur mon_image

### Liste des volumes
docker volume ls

### Suppression d'un volume
docker volume rm mon_volume
```

## Bind Mounts Docker

1. **Qu'est-ce qu'un Bind Mount ?** En Docker, un bind mount est un mécanisme qui permet de monter directement un répertoire ou un fichier de l'hôte dans un conteneur. Contrairement aux volumes, les bind mounts n'utilisent pas de gestionnaire de stockage propre à Docker, mais sont plutôt basés sur le montage du système de fichiers hôte dans le conteneur.
2. **Syntaxe du Bind Mount :** La syntaxe pour utiliser un bind mount lors du lancement d'un conteneur est la suivante : `docker run -v /chemin/sur/hote:/chemin/dans/conteneur mon_image`
3. **Avantages des Bind Mounts :
Partage en Temps Réel :** Les modifications dans le répertoire de l'hôte sont reflétées instantanément dans le conteneur et vice versa.
**Accès Direct :** Les fichiers sont accessibles et manipulables directement depuis l'hôte et le conteneur.
**Pas de Gestion Docker :** Les bind mounts ne nécessitent pas de gestion spécifique à Docker, ce qui les rend simples et flexibles.
4. **Bind Mounts vs Volumes :
Portabilité :** Les bind mounts dépendent étroitement de la structure du système de fichiers de l'hôte, ce qui peut les rendre moins portables que les volumes.
**Isolation :** Les bind mounts ne bénéficient pas de l'isolation propre aux volumes gérés par Docker.
**Complexité :** Les bind mounts sont plus simples à mettre en œuvre mais offrent moins de fonctionnalités de gestion que les volumes.
5. **Exemple Complet :**

```markdown
### Lancement d'un conteneur avec un bind mount
docker run -v /chemin/sur/hote:/chemin/dans/conteneur mon_image

### Modifications sur l'hôte sont immédiatement reflétées dans le conteneur
echo "Nouveau Contenu" > /chemin/sur/hote/fichier.txt

### Les modifications dans le conteneur sont reflétées sur l'hôte
cat /chemin/sur/hote/fichier.txt
```

**Utilisation Recommandée :** Les bind mounts sont souvent utilisés lors du développement pour permettre aux développeurs d'éditer du code sur l'hôte et de voir les modifications directement dans le conteneur.

**Conclusion :** Les bind mounts offrent une solution simple et directe pour partager des données entre l'hôte et le conteneur. Ils sont particulièrement utiles dans des environnements de développement où l'objectif est d'avoir un accès rapide et direct aux fichiers sur l'hôte à l'intérieur du conteneur. Cependant, pour des scénarios de production, les volumes Docker peuvent être préférés en raison de leur gestion plus robuste par Docker.

## Volumes vs Bind Mounts

### Différences Principales :

1. **Gestionnaire :**
    - **Volume :** Les volumes sont gérés par Docker. Ils ont des noms, peuvent être créés explicitement, et Docker s'occupe de leur cycle de vie.
    - **Bind Mount :** Les bind mounts dépendent du système de fichiers de l'hôte et ne sont pas gérés spécifiquement par Docker.
2. **Portabilité :**
    - **Volume :** Les volumes sont généralement plus portables, car ils sont gérés par Docker. On peut facilement partager des volumes entre différents conteneurs sur le même hôte ou même entre hôtes Docker.
    - **Bind Mount :** Les bind mounts dépendent de la structure du système de fichiers de l'hôte, ce qui peut rendre leur portabilité plus complexe.
3. **Isolation :**
    - **Volume :** Les volumes sont isolés et gérés par Docker, ce qui les rend plus robustes en termes de gestion des données persistantes.
    - **Bind Mount :** Les bind mounts partagent directement le système de fichiers de l'hôte, ce qui peut potentiellement conduire à une moins grande isolation.
4. **Utilisation Recommandée :**
    - **Volume :** Les volumes sont recommandés pour la persistance des données, le partage entre les conteneurs et la gestion facile des données.
    - **Bind Mount :** Les bind mounts sont utiles pour des situations spécifiques, comme le développement, où un accès direct et en temps réel aux fichiers de l'hôte est nécessaire dans le conteneur.

### Quand Utiliser Quoi ?

- **Utiliser les Volumes lorsque :**
    - On a besoin de conserver (persister) des données entre les redémarrages de conteneurs.
    - On veut partager des données entre plusieurs conteneurs.
    - On privilégie une gestion centralisée et isolée par Docker.
- **Utiliser les Bind Mounts lorsque :**
    - On développe localement et a besoin d'un accès direct aux fichiers sur l'hôte depuis le conteneur.
    - La gestion spécifique à Docker n'est pas une priorité, et la simplicité d'accès aux fichiers est plus critique.
    - On préfère une approche plus souple et moins contraignante pour le partage de données.

### Conclusion :

Le choix entre volumes et bind mounts dépend des besoins spécifiques du projet et de l'environnement d'utilisation. En règle générale, pour la persistance des données et le partage entre conteneurs, les volumes sont souvent plus adaptés. Cependant, dans des cas de développement local où l'accès direct aux fichiers est crucial, les bind mounts peuvent être la meilleure option.